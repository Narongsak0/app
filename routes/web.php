<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::middleware('auth')->group(function () {
    Route::get('contact/create', 'App\Http\Controllers\ContactController@create');
    Route::post('contact', 'App\Http\Controllers\ContactController@store');
    Route::get('contact', 'App\Http\Controllers\ContactController@index');
    Route::put('contact/{contact}', 'App\Http\Controllers\ContactController@update');
    Route::get('contact/{contact}', 'App\Http\Controllers\ContactController@show');
    Route::delete('contact/{contact}', 'App\Http\Controllers\ContactController@destroy');
    Route::get('contact/{contact}/edit', 'App\Http\Controllers\ContactController@edit');
});


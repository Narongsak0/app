@extends('layouts.app')
@section('content')
    <div class="container">
    @if ($errors->all())
        <ul class="alert alert-danger">
            @foreach ($errors->all() as $errors)
                <li>
                    {{$errors}}
                </li>
            @endforeach
        </ul>
        @endif
    {!! Form::open(['url' => 'contact', 'method' => 'POST']) !!}
    @csrf
        <div claa="col-md-6">
            <div class="form-group">
            {!! Form::label('NAME') !!}
            {!! Form::text('name',null,["class"=>"form-control"])!!}
            </div>
            <div class="form-group">
            {!! Form::label('EMAIL') !!}
            {!! Form::text('email',null,["class"=>"form-control"])!!}
            </div>
            <div class="form-group">
            {!! Form::label('Phone') !!}
            {!! Form::text('phone',null,["class"=>"form-control"])!!}
            </div>
            <input type="submit" value="บันทึก" class="btn btn-primary">
            <a href="/contact" class="btn btn-success">กลับ</a>
        <div>
    {!! Form::close() !!}
    </div>
@endsection
